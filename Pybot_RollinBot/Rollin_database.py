import sqlite3
from collections import Counter

# Добавление пользователя

def is_exists_Customers(user_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            result = cur.execute('SELECT * FROM `Customers` WHERE `user_id` = ?', (user_id,)).fetchall()
            return bool(len(result))
    except:
        return False


def add_Customers(user_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            cur.execute("INSERT INTO `Customers` (`user_id`, `bill`, `purchase`, `balance`) VALUES(?,?,?,?)",
                        (user_id, 0, 0, 0))
    except:
        pass

# Добавление товара

def add_merchant_Customers(category, title, desc, price, data):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            cur.execute("INSERT INTO `merchant` (`category`, `title`, `desc`, `price`, `data`) VALUES(?,?,?,?,?)",
                        (category, title, desc, price, data))
    except:
        pass


# Добавление в историю покупок

def add_history_Customers(user_id, category, datetime, summ):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            cur.execute("INSERT INTO `history` (`user_id`, `category`, `datebuy`, `sum`) VALUES(?,?,?,?)",
                        (user_id, category, datetime, summ))

            cur.execute("UPDATE `Customers` SET `bill` = bill + ? WHERE `user_id` = ?", (1, user_id))
            cur.execute("UPDATE `Customers` SET `purchase` = purchase + ? WHERE `user_id` = ?", (summ, user_id))
            cur.execute("UPDATE `Customers` SET `balance` = balance - ? WHERE `user_id` = ?", (summ, user_id))
    except:
        pass


# Получение значений

def is_exists_merchant_Customers(merchant_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            result = cur.execute('SELECT * FROM `merchant` WHERE `merchant_id` = ?', (merchant_id,)).fetchall()
            return bool(len(result))
    except:
        return False


def get_bill_Customers(user_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            result = cur.execute('SELECT * FROM `Customers` WHERE `user_id` = ?', (user_id,)).fetchall()
            for row in result:
                return row[1]
    except:
        pass


def get_purchase_Customers(user_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            result = cur.execute('SELECT * FROM `Customers` WHERE `user_id` = ?', (user_id,)).fetchall()
            for row in result:
                return row[2]
    except:
        pass


def get_balance_Customers(user_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            result = cur.execute('SELECT * FROM `Customers` WHERE `user_id` = ?', (user_id,)).fetchall()
            for row in result:
                return row[3]
    except:
        pass


def get_category_Customers():
    try:
        array = []

        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            rows = cur.execute("SELECT * FROM merchant").fetchall()

            for row in rows:
                if row[0] not in array:
                    array.append(row[0])

        return array
    except:
        pass


def get_merchant_Customers(category):
    try:
        array = []
        titleused = []

        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            rows = cur.execute("SELECT * FROM merchant WHERE category = ?", (category,)).fetchall()

            for row in rows:
                if row[1] not in titleused:
                    titleused.append(row[1])
                    array.append(f'{row[5]}:{row[1]} [{row[3]} ₽ - ')
                else:
                    titleused.append(row[1])

        merchant = []
        c = Counter(titleused)

        for row in array:
            regex = row.split(':')
            regex = regex[1].split(' ')

            merchant.append(f'{row} {c[regex[0]]} шт.]')

        return merchant
    except:
        pass


def get_historybuy_Customers(user_id):
    try:
        array = []

        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            rows = cur.execute("SELECT * FROM history WHERE user_id = ?", (user_id,)).fetchall()

            for row in rows:
                if len(array) != 10:
                    array.append(f'{row[1]} - {row[2]} - {row[3]} ₽')
                else:
                    return array

        return array
    except Exception as e:
        print(e)


def get_infomerchant_Customers(merchant_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            rows = cur.execute("SELECT * FROM merchant WHERE merchant_id = ?", (merchant_id,)).fetchall()

            for row in rows:
                return f'{row[1]}:{row[2]}:{row[3]}:{row[5]}'

    except Exception as e:
        print(e)


def get_usersId_Customers():
    try:
        array = []

        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            rows = cur.execute("SELECT * FROM Customers").fetchall()

            for row in rows:
                array.append(row[0])

        return array
    except Exception as e:
        print(e)


def get_fullMerchant_Customers():
    try:
        array = []

        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            rows = cur.execute("SELECT * FROM merchant").fetchall()

            for row in rows:
                array.append(f'{row[5]}: {row[0]} - {row[1]} - {row[2]} - {row[3]} - {row[4]}')

        return array
    except:
        pass


# Удаление значений

def delete_merchant_Customers(merchant_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            cur.execute("DELETE FROM `merchant` WHERE `merchant_id` = ?", (merchant_id,))
    except:
        pass


def get_summerchant_Customers(merchant_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            result = cur.execute('SELECT * FROM `merchant` WHERE `merchant_id` = ?', (merchant_id,)).fetchall()
            for row in result:
                return row[3]
    except:
        pass


def get_datamerchant_Customers(merchant_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            result = cur.execute('SELECT * FROM `merchant` WHERE `merchant_id` = ?', (merchant_id,)).fetchall()
            for row in result:
                return row[4]
    except:
        pass


def get_titlemerchant_Customers(merchant_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            result = cur.execute('SELECT * FROM `merchant` WHERE `merchant_id` = ?', (merchant_id,)).fetchall()
            for row in result:
                return row[1]
    except:
        pass


def get_merchantid_Customers(title):
    try:
        array = []
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            result = cur.execute('SELECT * FROM `merchant` WHERE `title` = ?', (title,)).fetchall()
            for row in result:
                array.append(row[5])

        return array
    except:
        pass


def get_categorymerchant_Customers(merchant_id):
    try:
        with sqlite3.connect("Sushi_database.db") as con:
            cur = con.cursor()
            result = cur.execute('SELECT * FROM `merchant` WHERE `merchant_id` = ?', (merchant_id,)).fetchall()
            for row in result:
                return row[0]
    except:
        pass